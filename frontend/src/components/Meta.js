import React from 'react';
import { Helmet } from 'react-helmet';

const Meta = ({ title, description, keywords }) => {
  return (
    <Helmet>
      <title>{title}</title>
      <meta name='description' content={description} />
      <meta name='keywords' content={keywords} />
    </Helmet>
  );
};

Meta.defaultProps = {
  title: 'Spring Balancers Online Store',
  description:
    'Online eStore for Spring Balancers, Tool Balancers, Load Balancers by Powermaster in USA',
  keywords:
    'spring balancer, Tool Balancers, load balancer, spring balancers, Tool Balancer, load balancers, powermaster, SWF series spring balancers, SWA series spring balancers',
};

export default Meta;
