import bcrypt from 'bcryptjs';

const users = [
  {
    name: 'Admin User',
    email: 'admin@example.com',
    password: bcrypt.hashSync('123456', 10),
    isAdmin: true,
  },
  {
    name: 'Asfahan Shaikh',
    email: 'asfahan@example.com',
    password: bcrypt.hashSync('123456', 10),
  },
  {
    name: 'Nahafsa Shaikh',
    email: 'nahafsa@example.com',
    password: bcrypt.hashSync('123456', 10),
  },
];

export default users;
